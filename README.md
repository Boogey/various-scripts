# Various scripts sh #

### Summary ###

* This repo contains multiple scripts for various uses

### How do I get set up? ###

Simply launch the scripts. Here's some of the stuff what you can find in this repo :

* Script to edit images iOS style (needs imagemagick)
* Git commands for multiple repos at once (useful for white label apps)
* XCode files edition